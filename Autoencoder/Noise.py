# -*- coding: utf-8 -*-
"""
Created on Tue Apr 19 16:23:03 2016

@author: thewa
"""
import numpy as np
#np.random.seed(1)
def saltpepper(image,lower,upper):
    
    size = image.size
    out = np.array(image)
    #about 5% of switched features
    r = np.array(np.random.randint(0,30,size=size))
    for i in range(0,r.size):
        if r[i] == 5:
            out[i] = lower
        if r[i] == 6:
            out[i] = upper 
    return out
    