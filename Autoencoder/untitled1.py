# -*- coding: utf-8 -*-
"""
Created on Fri May 27 16:37:38 2016

@author: thewa
"""

# -*- coding: utf-8 -*-

import numpy as np
import time

npr = 10000
mpr = 10
af_size = 40

# Fake initialization of variables
grad = np.zeros(af_size)
G2 = np.random.rand(npr*mpr, 4)
I2 = np.random.randint(low=0, high=af_size-4, size=(npr, mpr))
error = np.random.rand(npr, mpr)
offs = 0

start = time.clock()

#accumulate gradients for layer 2
for row in range(0, npr):
    for col in range(0, mpr):
        grad[I2[row, col]:I2[row, col] + 4] += G2[offs]*(error[row,col])
        offs += 1
        
end = time.clock()
print(end - start)

# Preinitialize the gradients
#  - Each row is a single input/ouput pair
#  - Each column contains the gradients for the AF
grad2 = np.zeros((npr*mpr, af_size))

start = time.clock()

# Multiply G2 by the error (reshaped as a vector)
# Note the use of broadcasting here
grad_tmp = (G2*error.reshape(error.size, 1))

# Vectorize the indices
lin_indices = I2.ravel()

# Assign sequentially all gradients
grad2[np.arange(npr*mpr), lin_indices] = grad_tmp[:, 0]
grad2[np.arange(npr*mpr), lin_indices+1] = grad_tmp[:, 1]
grad2[np.arange(npr*mpr), lin_indices+2] = grad_tmp[:, 2]
grad2[np.arange(npr*mpr), lin_indices+3] = grad_tmp[:, 3]

# Sum per rows
grad2 = grad2.sum(axis=0)

end = time.clock()
print(end - start)