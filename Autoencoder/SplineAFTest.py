# -*- coding: utf-8 -*-
import numpy as np


"""
09/05/2016

Backup of SplineAF.py before vectorization
"""



class SplineAFTest:
    
    def __init__(self,limit,sampling,Spline_Type):
        #column vector of control points
    
        self.xs = np.arange(-limit,limit+sampling,sampling)
        self.size = np.size(self.xs) 
        self.ys = np.empty(self.size)
        self.sampling = sampling
        if Spline_Type == 'CR':
            self.C = np.array([[-1.0,3.0,-3.0,1.0],[2.0,-5.0,4.0,-1.0],[-1.0,0.0,1.0,0.0],[0.0,2.0,0.0,0.0]])*0.5
        
        for i in range(0,self.size):
            self.ys[i] = np.tanh(self.xs[i])
            
    def getI(self,s):

        l = np.size(self.ys)
        Su = s/self.sampling + (l-1)/2       
        i = np.floor(Su)       
        u = Su - i
        i = i - 1       
        if i<0:
            i = 0
        if (i>(l-4)):
            i = l-4
        return (u,i) 
        
    def getValue(self,s):
        u,i = self.getI(s)
        g = np.array(self.C.T.dot([u**3, u**2, u, 1]))
        x = g.dot((self.ys[i:i+4]))
        return (x,u,i,g)
    
    def derivative(self,s,u,i):
            # Compute the derivative of the spline.
            # Additional output arguments are:
            #- u, uIndex: identical to compute_value(s).
            #- g: derivative coordinate vector given by [3u^2 2u 1 0].
            # It is possible to provide u and uIndex as additional
            # arguments to speed up the computation, if they have already
            # been computed previously. 
        #u,i = self.getI(s)
        g = np.array([3*u**2, 2*u, 1, 0]).T.dot(self.C)
        dx = g.dot(self.ys[i:i+4])/self.sampling
        return (dx,u,i,g)
        


        