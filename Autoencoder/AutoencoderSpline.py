# -*- coding: utf-8 -*-
"""Created on Fri Mar 11 16:29:50 2016
@author: thewa
"""
import numpy as np
import scipy.io
from sklearn import datasets
from sklearn import preprocessing
from sklearn.metrics import mean_squared_error
from SplineAF import SplineAF
from SplineAFTest import SplineAFTest
from sklearn.datasets import load_digits
import matplotlib.pyplot as plt
from matplotlib.legend_handler import HandlerLine2D
import Noise as N
import time
import csv
from sklearn.grid_search import ParameterGrid
from mnist import load_mnist

def getSplineSample(af):
    rang = np.arange(-2,2.2,0.02)
    Q1 = []
    U = []
    I = []
    G = []
    for r in rang:
        x, u, i, g = af.getValue(r)
        Q1.append(x)
        U.append(u)
        I.append(i)
        G.append(np.tanh(r))
    return Q1,G,rang

def updatePoints(error, I2, G2, af2, lambdQ):
    npr, mpr = np.shape(error)
    grad = np.zeros(af2.ys.size)        
    grad2 = np.zeros((npr*mpr, af2.ys.size))
    
    # Multiply G2 by the error (reshaped as a vector)
    # Note the use of broadcasting here
    grad_tmp = (G2*error.reshape(error.size, 1))
    
    # Vectorize the indices
    lin_indices = I2.ravel().astype('int')
    
    # Assign sequentially all gradients
    grad2[np.arange(npr*mpr), lin_indices] = grad_tmp[:, 0]
    grad2[np.arange(npr*mpr), lin_indices+1] = grad_tmp[:, 1]
    grad2[np.arange(npr*mpr), lin_indices+2] = grad_tmp[:, 2]
    grad2[np.arange(npr*mpr), lin_indices+3] = grad_tmp[:, 3]
    
    # Sum per rows
    grad = grad2.sum(axis=0)    
    grad += lambdQ*(af2.ys-af2.ys0)
    return grad
        
def testPass(a, inDataTest, inDataTestN, testErr, frac):
    n,m = np.shape(inDataTestN)
    preH = np.concatenate((inDataTestN, np.ones((n, 1))), axis=1).dot(a.inW)
    npr, mpr = np.shape(preH)
    H, U1, I1, G1 = af1.getValueM(preH)
    preoutL = np.concatenate((H, np.ones((npr, 1))), axis=1).dot(a.outW)
    outL, U2, I2, G2 = af2.getValueM(preoutL)
    testErr.append(np.sqrt(mean_squared_error(inDataTest,outL)))
    return testErr

  
class Autoencoder:
    def __init__(self, inputData, nHidden):
        self.inData = inputData
        n, m = self.inData.shape
        self.inW= np.random.random_sample((m+1, nHidden))*0.001
        self.outW = np.random.random_sample((nHidden+1, m))*0.001
#@profile        
#def start():
start_time = time.clock()

db = 'digits'

if db == 'iris':
    iris = datasets.load_iris()
    inData = iris.data
    inDataN = inData
    scalerD = preprocessing.MinMaxScaler((-0.8,0.8))
    scalerN = preprocessing.MinMaxScaler((-0.8,0.8))
    inData = scalerD.fit_transform(inData)
    inDataN = scalerN.fit_transform(inDataN)
if db == 'bodyfat':
    bodyfat = scipy.io.loadmat('matlab.mat')
    inData = bodyfat['x']
    inData = inData.T
    inDataN = inData
    scalerD = preprocessing.MinMaxScaler((-0.8,0.8))
    scalerN = preprocessing.MinMaxScaler((-0.8,0.8))
    inData = scalerD.fit_transform(inData)
    inDataN = scalerN.fit_transform(inDataN)

if db == 'digits':
    digits = load_digits()
    inData = digits.data[0:1500]
    inDataN = inData
    scalerD = preprocessing.MinMaxScaler((0,1))
    scalerN = preprocessing.MinMaxScaler((0,1))
    inData = scalerD.fit_transform(inData)
    inDataN = scalerN.fit_transform(inDataN)       
if db == 'mnist':
    num_images = 18000
    inData = load_mnist()[0][0:num_images].reshape(num_images,784).astype(np.float64)
    inData /= 255
    inDataN = inData

n, m = inData.shape

"""
print "adding s&p noise.."
for i in range(0,n):
    inDataN[i] = N.saltpepper(inData[i],-0.8,0.8)
print "Done"
diff = np.abs(inData - inDataN)
diff = diff.flatten()
differents = 0
for i in range(0,diff.size):
    if diff[i] != 0:
        differents +=1


"""
"""
noise = np.random.normal(0, 1, n*m).reshape(n, m)*0.01
inDataN = inData + noise
"""

inDataFull = inData 
inDataFullN = inDataN 

nHidden = 100
frac = 1
nHidden = nHidden/frac
saveobjF = 0
dic = {'Testnum': [], 'MinTestVal': [], 'Lambda': [], 'LambdaQ': []}
param_grid = {'Lambda': [0.1, 0.01, 0.001, 0.0001, 0.00001], 'LambdaQ' : [0.1, 0.01, 0.001, 0.0001, 0.00001]}
grid = ParameterGrid(param_grid)
for params in grid:
    lambd = 0.00001
    lambdQ = 0.01
    for test in range(0, 3):
        #3-fold-cross validation
        ind = np.arange(0, 3)
        piece = 84
        mintests = []
        dic['Lambda'].append(lambd)
        dic['LambdaQ'].append(lambdQ)
        dic['Testnum'].append(test)
        np.random.seed(1)               #keeping the same shifts for both sets
        np.random.shuffle(inDataFull)   #keeping the same shifts for both sets
        np.random.seed(1)               #keeping the same shifts for both sets
        np.random.shuffle(inDataFullN)  #keeping the same shifts for both sets
        np.random.RandomState()         #setting a random state
        for kval in ind:
            inData = inDataFull
            inDataN = inDataFullN
            if kval == 0:
                inDataTest = inData[piece*(kval+2):piece*(kval+3)]
                inDataTestN = inData[piece*(kval+2):piece*(kval+3)]
                inData = inData[piece*kval:piece*(kval+2)]
                inDataN = inDataN[piece*kval:piece*(kval+2)]
    
            if kval == 1:
                inDataTest = inData[0:piece]
                inDataTestN = inDataN[0:piece]
                inData = inData[piece*kval:piece*(kval+2)]
                inDataN = inDataN[piece*kval:piece*(kval+2)]            
    
            if kval == 2:
                inDataTest = inData[piece:piece*kval]
                inDataTestN = inDataN[piece:piece*kval]
                inData = np.concatenate((inData[0:piece],inData[piece*(kval):piece*(kval+1)]))
                inDataN = np.concatenate((inDataN[0:piece],inDataN[piece*(kval):piece*(kval+1)]))
                
                
            a = Autoencoder(inDataN, nHidden)
            iters = 1500
            err = []
            err2 = []
            objF = []
            af1 = SplineAF(2, 0.2, 'CR')
            af2 = SplineAF(2, 0.2, 'CR')
            miqV = []
            batchSize= piece/3
            n, m = inData.shape
            indexBatch = np.arange(0,n)
            Gk1 = 0
            Gk2 = 0
            Gq1 = 0
            Gq2 = 0
            sigma = 0.00000001    
            testErr = []
            for k in range(0, iters):
                if k%500 == 0:
                    print k
                np.random.shuffle(indexBatch)                                    
                testErr = testPass(a,inDataTest, inDataTestN, testErr, frac)    
                for b in range(0, n/batchSize):
                    batchIndex = indexBatch[batchSize*b:batchSize*(b+1)]
                    #forward pass
                    preH = np.concatenate((a.inData[batchIndex], np.ones((batchSize, 1))), axis=1).dot(a.inW)
                    #print "preH ", preH
                    npr, mpr = np.shape(preH)
                    CODIO, CODIO2, CODIO3 = getSplineSample(af1)
                    #vectorized forward pass layer 1
                    H, U1, I1, G1 = af1.getValueM(preH)     
                    index = 0
                    mask1 = np.random.binomial(1, frac, size=(npr,mpr))
                    H*= mask1*(1/frac)
                    preoutL = np.concatenate((H, np.ones((batchSize, 1))), axis=1).dot(a.outW)
                    #vectorized forward pass layer 2
                    outL, U2, I2, G2 = af2.getValueM(preoutL)
                    
                    error = (outL - inData[batchIndex])/batchSize
                    if saveobjF:
                        W = np.concatenate((a.inW.flatten(), a.outW.flatten()), axis=0)
                        Q = np.concatenate((af1.ys-af1.ys0, af2.ys-af2.ys0), axis = 0)
                        objF.append(np.sum(np.power(inData[batchIndex]-outL,2))/(2.0*batchSize) + lambd*np.linalg.norm(W)/2.0 + lambdQ*np.linalg.norm(Q)/2.0)
                    
                    #backward pass    
                    #output-hidden layer gradients
                    dx2 = af2.derivativeM(preoutL, U2, I2)
            
                    grad = updatePoints(error, I2, G2, af2, lambdQ)
                    #Adagrad for ctrl points
                    sigma = 0.00000001
                    Gq2 += grad**2          
                    af2.ys += -0.01/(np.sqrt(Gq2)+sigma)*(grad)
                    
                    #Vanilla SGD for ctrl points
                    #af2.ys -= 0.005*grad
                    #calculate delta for out-hidden connections
                    delta = error*(dx2)
                    outL_grad = (np.concatenate((H, np.ones((batchSize, 1))), axis=1)).T.dot(delta) + lambd*a.outW     
                    #hidden-input layer gradients
                    npr, mpr = np.shape(preH)
                    dx1 = np.empty([npr, mpr])
                 
                    #multiply delta with the hidden weights matrix except bias connection
                    delta = (delta.dot(a.outW.T[:,:nHidden]))
                    grad = np.zeros(af1.ys.size)
                    
                    dx1 = af1.derivativeM(H, U1, I1)
                    """
                    grad2 = np.zeros((npr*mpr, af1.ys.size))
                    
                    # Multiply G2 by the error (reshaped as a vector)
                    # Note the use of broadcasting here
                    grad_tmp = (G1*delta.reshape(delta.size, 1))
                    
                    # Vectorize the indices
                    lin_indices = I1.ravel().astype('int')
                    
                    # Assign sequentially all gradients
                    grad2[np.arange(npr*mpr), lin_indices] = grad_tmp[:, 0]
                    grad2[np.arange(npr*mpr), lin_indices+1] = grad_tmp[:, 1]
                    grad2[np.arange(npr*mpr), lin_indices+2] = grad_tmp[:, 2]
                    grad2[np.arange(npr*mpr), lin_indices+3] = grad_tmp[:, 3]
                    
                    # Sum per rows
                    grad = grad2.sum(axis=0)
                    
                    """
                    """
                    offs=0
                    for row in range(0, npr):
                        for col in range(0, mpr):
                            grad[I1[row, col]:I1[row, col] + 4] += G1[offs]*(delta[row,col])
                            offs += 1
                    
                    offs = 0
                    """
                    """
                    grad += lambdQ*(af1.ys-af1.ys0)
                    """
                    grad = updatePoints(delta, I1, G1, af1, lambdQ)
                    #adagrad for ctrl points   
                    Gq1 += grad**2        
                    af1.ys += -0.01/(np.sqrt(Gq1)+sigma)*(grad)          
                    
                    #vanilla SGD for ctrl points
                    #af1.ys -= 0.005*grad
                    
                    #update delta for hidden input connections
                    delta = delta*dx1
                    Hgrad = (np.concatenate((a.inData[batchIndex], np.ones((batchSize, 1))), axis=1).T.dot(delta)) + lambd*a.inW 
                    
                    #vanilla SGD update  
                    #a.inW -= 0.0001*0.5*(Hgrad)
                    #a.outW -= 0.0001*0.5*(outL_grad)
                    
                    Gk1 += Hgrad**2
                    a.inW += -0.01/(np.sqrt(Gk1)+sigma)*(Hgrad)
                    
                    Gk2 += outL_grad**2
                    a.outW += -0.01/(np.sqrt(Gk2)+sigma)*(outL_grad)
                                   
                    err.append((mean_squared_error(inData[batchIndex], outL)))
                    err2.append(np.sqrt(((inData[batchIndex]-outL)**2).mean()))
            print "Test n: ", test, " lambdaQ: ", lambdQ, " lambda: ", lambd
            print "MSE train: ", err[len(err)-1]
            print "RMSE train: ", err2[len(err2)-1]
            print "RMSE test: ", testErr[len(testErr)-1]
            print "Min RMSE test: ", min(testErr)
            mintests.append(min(testErr))
            
            #print "FVAL: ", objF[len(objF)-1]
            #np.random.seed(1)
        dic['MinTestVal'].append(mintests)
#save splines point and plot errors and splines     
ys2 = af2.ys
xs2 = af2.xs
spline2 = np.vstack((xs2, ys2))   
ys1 = af1.ys
xs1 = af1.xs
ys0 = af1.ys0

with open('names.csv', 'w') as csvfile:
    fieldnames = dic.keys
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writerow(dic)
   
   
"""
test = 0
plt.figure()
Q1, G, rang = getSplineSample(af1)
plt.plot(rang,Q1, label="Spline 1 Test n. %s"%test)
plt.legend()
plt.figure()
Q2, G, rang = getSplineSample(af2)
plt.plot(rang,Q2, label="Spline 2 Test n. %s"%test)
plt.legend()
plt.figure()
"""
"""    
ysDiff = []   
for l in range(0,len(G)):
    ysDiff.append(G[l] - Q1[l])
"""
"""
plt.plot(testErr,label="Test Error    lambda: %f" %lambd)
plt.plot(err2,label="Train Error    lambdaQ: %f" %lambdQ)
plt.plot(nHidden ,label="Hidden neurons : %f" %nHidden)
plt.legend()
"""


print("--- %s seconds ---" % (time.clock() - start_time))