# -*- coding: utf-8 -*-
"""
Created on Thu Jun 09 16:59:25 2016

@author: thewa
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import expit

a = np.array([[1,2,3], [4,5,6]])

print np.linalg.norm(a)
print np.linalg.norm(a.ravel())