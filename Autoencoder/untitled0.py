import accelerate
import numpy as np
import timeit
def gpu():
    x = np.array(np.arange(50000000)).astype(np.float64)
    b = accelerate.cuda.blas.Blas()
    res = b.dot(x,x)
def cpu():
    x = np.array(np.arange(50000000)).astype(np.float64)
    res = np.dot(x,x)
    
print timeit.timeit(cpu,  number=1)/1