# -*- coding: utf-8 -*-
"""
Created on Wed Jul 06 13:08:20 2016

@author: thewa
"""
import numpy as np

class AutoencoderClass:
    def __init__(self, inputData, nHidden):
        self.inData = inputData
        n,m = self.inData.shape
        self.inW = np.random.random_sample((m+1,nHidden))*0.001
        self.outW = np.random.random_sample((nHidden+1,m))*0.001