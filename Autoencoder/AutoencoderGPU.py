# -*- coding: utf-8 -*-
"""Created on Fri Mar 11 16:29:50 2016
@author: thewa
"""
import numpy as np
import scipy.io
from sklearn import datasets
from sklearn import preprocessing
from sklearn.metrics import mean_squared_error
from SplineAF import SplineAF
from SplineAF_ALL import SplineAF_ALL
from SplineAFTest import SplineAFTest
from sklearn.datasets import load_digits
import matplotlib.pyplot as plt
from matplotlib.legend_handler import HandlerLine2D
import Noise as N
import time

def getSplineSample(af, neuron):
    rang = np.arange(-2,2.2,0.02)
    Q1 = []
    U = []
    I = []
    G = []
    for r in rang:
        x, u, i, g = af.getValueMsingle(r, neuron)
        Q1.append(x)
        U.append(u)
        I.append(i)
        G.append(np.tanh(r))
    return Q1,G,rang

def updatePoints(d, I, G, af, lambdQ):
    npr, mpr = np.shape(d)     
    grad2 = np.zeros((npr*mpr, af.size))
    
    # Multiply G2 by the error (reshaped as a vector)
    # Note the use of broadcasting here
    grad_tmp = (G*d.reshape(d.size, 1))
    
    # Vectorize the indices
    lin_indices = I.ravel().astype('int')
    
    # Assign sequentially all gradients
    grad2[np.arange(npr*mpr), lin_indices] = grad_tmp[:, 0]
    grad2[np.arange(npr*mpr), lin_indices+1] = grad_tmp[:, 1]
    grad2[np.arange(npr*mpr), lin_indices+2] = grad_tmp[:, 2]
    grad2[np.arange(npr*mpr), lin_indices+3] = grad_tmp[:, 3]
    
    # Sum per rows
    grad2Rows, grad2Cols = np.shape(grad2)
    grad = np.sum(np.reshape(grad2, ((npr, af.size*(grad2Rows/npr)))), axis = 0)
    """
    #accumulate gradients for layer 2
    for row in range(0, npr):
        for col in range(0, mpr):
            grad[I2[row, col]:I2[row, col] + 4] += G2[offs]*(error[row,col])
            offs += 1
       
    offs = 0
    """
    grad = grad.reshape(grad2Rows/npr, 21)
    grad += lambdQ*(af.ys-af.ys0)
    return grad
        
def testPass(a, inDataTest, inDataTestN, testErr, frac):
    n,m = np.shape(inDataTestN)
    preH = np.concatenate((inDataTestN, np.ones((n, 1))), axis=1).dot(a.inW)
    npr, mpr = np.shape(preH)
    H, U1, I1, G1 = af1.getValueM(preH)
    preoutL = np.concatenate((H, np.ones((npr, 1))), axis=1).dot(a.outW)
    outL, U2, I2, G2 = af2.getValueM(preoutL)
    testErr.append(np.sqrt(mean_squared_error(inDataTest,outL)))

  
class Autoencoder:
    def __init__(self, inputData, nHidden):
        self.inData = inputData
        n, m = self.inData.shape
        self.inW= np.random.random_sample((m+1, nHidden))*0.1
        self.outW = np.random.random_sample((nHidden+1, m))*0.1
#@profile        
#def start():
start_time = time.clock()

#iris = datasets.load_iris()
#inData = iris.data

bodyfat = scipy.io.loadmat('matlab.mat')
inData = bodyfat['x']
inData = inData.T

"""
digits = load_digits()
inData = digits.data[0:1797]
"""
inDataN = inData
n, m = inData.shape
scalerD = preprocessing.MinMaxScaler((-0.8, 0.8))
scalerN = preprocessing.MinMaxScaler((-0.8, 0.8))
inData = scalerD.fit_transform(inData)
inDataN = scalerN.fit_transform(inDataN)
"""
print "adding s&p noise.."
for i in range(0,n):
    inDataN[i] = N.saltpepper(inData[i],-0.8,0.8)
print "Done"
diff = np.abs(inData - inDataN)
diff = diff.flatten()
differents = 0
for i in range(0,diff.size):
    if diff[i] != 0:
        differents +=1

"""
"""
noise = np.random.normal(0, 1, n*m).reshape(n, m)*0.01
inDataN = inData + noise
"""

nHidden = 50
frac = 1
nHidden = nHidden/frac
inDataTest = inData[200:250]
inDataTestN = inDataN[200:250]
inData = inData[0:200]
inDataN = inDataN[0:200]
a = Autoencoder(inDataN, nHidden)
iters = 1500
err = []
err2 = []
objF = []
n, m = inData.shape
af1 = SplineAF_ALL(2, 0.2, 'CR', nHidden)
af2 = SplineAF_ALL(2, 0.2, 'CR', m)
miqV = []
batchSize= 50

indexBatch = np.arange(0,n)
Gk1 = 0
Gk2 = 0
Gq1 = 0
Gq2 = 0
sigma = 0.00001

lambd = 0.00001
lambdQ = 0.001
testErr = []

"""
lambdList = [1.0,0.1,0.01,0.001,0.0001,0.00001,0.000001,0.0000001,0.00000001,0.000000001,0.0000000001]
lambdQList = [1.0,0.1,0.01,0.001,0.0001,0.00001,0.000001,0.0000001,0.00000001,0.000000001,0.0000000001]
tests = len(lambdQList)
"""
for k in range(0, iters):
    if k%500 == 0:
        print k
    np.random.shuffle(indexBatch)
    for b in range(0, n/batchSize):
        batchIndex = indexBatch[batchSize*b:batchSize*(b+1)]
        #forward pass
        preH = np.concatenate((a.inData[batchIndex], np.ones((batchSize, 1))), axis=1).dot(a.inW)
        #print "preH ", preH
        npr, mpr = np.shape(preH)
    
        #vectorized forward pass layer 1
        H, U1, I1, G1 = af1.getValueM(preH)  
        index = 0
        mask1 = np.random.binomial(1, frac, size=(npr,mpr))
        H*= mask1*(1/frac)
        preoutL = np.concatenate((H, np.ones((batchSize, 1))), axis=1).dot(a.outW)
        #vectorized forward pass layer 2
        outL, U2, I2, G2 = af2.getValueM(preoutL)
        
        error = (outL - inData[batchIndex])/batchSize
        W = np.concatenate((a.inW.flatten(), a.outW.flatten()), axis=0)
        Q = np.concatenate((af1.ys-af1.ys0, af2.ys-af2.ys0), axis = 0)
        objF.append(np.sum(np.power(inData[batchIndex]-outL,2))/(2.0*batchSize) + lambd*np.linalg.norm(W)/2.0 + lambdQ*np.linalg.norm(Q)/2.0)
        
        testPass(a,inDataTest, inDataTestN, testErr, frac)    
        
        
        #backward pass    
        #output-hidden layer gradients
        dx2 = af2.derivativeM(preoutL, U2, I2)

        grad = updatePoints(error, I2, G2, af2, lambdQ)
        #Adagrad for ctrl points
        sigma = 0.00001
        Gq2 += grad**2          
        af2.ys += -0.01/(np.sqrt(Gq2)+sigma)*(grad)
        
        #Vanilla SGD for ctrl points
        #af2.ys -= 0.005*grad
        #calculate delta for out-hidden connections
        delta = error*(dx2)
        outL_grad = (np.concatenate((H, np.ones((batchSize, 1))), axis=1)).T.dot(delta) + lambd*a.outW     
        #hidden-input layer gradients
        npr, mpr = np.shape(preH)
        dx1 = np.empty([npr, mpr])
     
        #multiply delta with the hidden weights matrix except bias connection
        delta = (delta.dot(a.outW.T[:,:nHidden]))
        grad = np.zeros(af1.ys.size)
        
        dx1 = af1.derivativeM(H, U1, I1)


        grad = updatePoints(delta, I1, G1, af1, lambdQ)
        #adagrad for ctrl points   
        Gq1 += grad**2        
        af1.ys += -0.01/(np.sqrt(Gq1)+sigma)*(grad)          
        
        #vanilla SGD for ctrl points
        #af1.ys -= 0.005*grad
        
        #update delta for hidden input connections
        delta = delta*dx1
        Hgrad = (np.concatenate((a.inData[batchIndex], np.ones((batchSize, 1))), axis=1).T.dot(delta)) + lambd*a.inW 
        
        #vanilla SGD update  
        #a.inW -= 0.0001*0.5*(Hgrad)
        #a.outW -= 0.0001*0.5*(outL_grad)
        
        Gk1 += Hgrad**2
        a.inW += -0.1/(np.sqrt(Gk1)+sigma)*(Hgrad)
        
        Gk2 += outL_grad**2
        a.outW += -0.1/(np.sqrt(Gk2)+sigma)*(outL_grad)
                       
        err.append((mean_squared_error(inData[batchIndex], outL)))
        err2.append(np.sqrt(((inData[batchIndex]-outL)**2).mean()))
    
#save splines point and plot errors and splines     
ys2 = af2.ys
xs2 = af2.xs
spline2 = np.vstack((xs2, ys2))   
ys1 = af1.ys
xs1 = af1.xs
ys0 = af1.ys0

test = 0
plt.figure()
for neuron in range(0, 10):
    Q1, G, rang = getSplineSample(af1, neuron)
    plt.plot(rang, Q1, label="Spline 1 Test n. %s"%test)
    plt.legend()
    plt.figure()
    Q2, G, rang = getSplineSample(af2, neuron)
    plt.plot(rang, Q2, label="Spline 2 Test n. %s"%test)
    plt.legend()
    plt.figure()
"""    
ysDiff = []   
for l in range(0,len(G)):
    ysDiff.append(G[l] - Q1[l])
"""
plt.plot(testErr,label="Test Error    lambda: %f" %lambd)
plt.plot(err2,label="Train Error    lambdaQ: %f" %lambdQ)
plt.plot(nHidden ,label="Hidden neurons : %f" %nHidden)
plt.legend()

print "Test n: ", test, " lambdaQ: ", lambdQ, " lambda: ", lambd
print "MSE train: ", err[len(err)-1]
print "RMSE train: ", err2[len(err2)-1]
print "RMSE test: ", testErr[len(testErr)-1]
print "FVAL: ", objF[len(objF)-1]

print("--- %s seconds ---" % (time.clock() - start_time))