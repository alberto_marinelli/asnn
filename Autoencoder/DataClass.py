# -*- coding: utf-8 -*-
"""
Created on Wed Jul 06 13:13:06 2016

@author: thewa
"""

import numpy as np
import scipy.io
from sklearn import datasets
from sklearn import preprocessing
from sklearn.datasets import load_digits
from mnist import load_mnist



def load(db):
    if db == 'iris':
        iris = datasets.load_iris()
        inData = iris.data
        inDataN = inData
        scalerD = preprocessing.MinMaxScaler((-0.8,0.8))
        scalerN = preprocessing.MinMaxScaler((-0.8,0.8))
        inData = scalerD.fit_transform(inData)
        inDataN = scalerN.fit_transform(inDataN)
    if db == 'bodyfat':
        bodyfat = scipy.io.loadmat('matlab.mat')
        scalerD = preprocessing.MinMaxScaler((-0.8,0.8))
        scalerN = preprocessing.MinMaxScaler((-0.8,0.8))
        rawData = bodyfat['x'].T
        inDataAll = scalerD.fit_transform(rawData)
        inData = inDataAll[0:210]
        inDataN = inDataAll[0:210]
        inDataTest = inDataAll[210:252]
        inDataTestN = inDataAll[210:252]
    if db == 'digits':
        #lambda for digits: 0.0001
        digits = load_digits()
        inData = digits.data[0:500]
        inDataTest = digits.data[500:700]
        Y = digits.target[0:500]
        YTest = digits.target[500:700]
        inDataN = inData
        scalerD = preprocessing.MinMaxScaler((0,1))
        scalerN = preprocessing.MinMaxScaler((0,1))
        inData = scalerD.fit_transform(inData)
        inDataTest = scalerD.fit_transform(inDataTest)
        inDataN = scalerN.fit_transform(inDataN)       
    if db == 'mnist': #60000x784 + 10000x784
        num_images = 1000
        inData = load_mnist()[0][0:num_images].reshape(num_images,784).astype(np.float64)
        inData /= 255
        inDataN = inData
    if db == 'chemical': #chemical is 498x8
        chem = scipy.io.loadmat('MATLAB_chemical.mat')
        scalerD = preprocessing.MinMaxScaler((-1,1))
        scalerT = preprocessing.MinMaxScaler((-0.5,0.5))
        np.random.seed(1)
        rawData = chem['X']
        np.random.shuffle(rawData)
        np.random.seed(1)
        Ys = chem['Y']
        np.random.shuffle(Ys)
        inDataAll = scalerD.fit_transform(rawData)
        inData = inDataAll[0:400]
        inDataTest = inDataAll[400:498]
        Ys = scalerT.fit_transform(Ys)
        Y = Ys[0:400]
        YTest = Ys[400:498]
    if db == 'calhousing':
        call = scipy.io.loadmat('statlib_calhousing.mat')
        scalerD = preprocessing.MinMaxScaler((-1,1))
        scalerT = preprocessing.MinMaxScaler((-0.5,0.5))
        np.random.seed(1)
        rawData = call['X']
        np.random.shuffle(rawData)
        np.random.seed(1)
        Ys = call['Y']
        np.random.shuffle(Ys)
        inDataAll = scalerD.fit_transform(rawData)
        inData = inDataAll[0:500]
        inDataTest = inDataAll[15000:15500]
        Ys = scalerT.fit_transform(Ys)
        Y = Ys[0:500]
        YTest = Ys[15000:15500]
    return inData, inDataTest, Y, YTest