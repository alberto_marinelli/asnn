# -*- coding: utf-8 -*-
"""
Created on Wed Jul 06 17:06:31 2016

@author: thewa
"""
import numpy as np
import pylab as pl 
from scipy.stats import norm
from scipy.special import expit
import scipy.io
from sklearn import datasets
from sklearn import preprocessing, metrics
from sklearn.metrics import mean_squared_error
from sklearn.datasets import load_digits
import matplotlib.pyplot as plt
import Noise as N
import time
from sklearn.grid_search import ParameterGrid
from mnist import load_mnist
from sklearn.datasets import load_sample_images
from AutoencoderClass import AutoencoderClass
import DataClass
from SplineAF_ALL import SplineAF_ALL
from sklearn.linear_model import LogisticRegression
import softmax
def grad_activation(n):
    #sigmoid    
    #return expit(n)*(1-expit(n))
    #tanh
    y = np.tanh(n)
    return (1-y**2)

class Autoencoder:
    def __init__(self, inputData, nHidden, lambd, lambdQ, saveobjF):
        self.inData = inputData
        self.lambd = lambd
        self.lambdQ = lambdQ
        self.saveobjF = saveobjF
        n, m = self.inData.shape
        self.nHidden = nHidden
        self.inW= np.random.random_sample((m+1, self.nHidden))*0.001
        self.outW = np.random.random_sample((self.nHidden+1, m))*0.001

    def getSplineSample(self, af, neuron):
        rang = np.arange(-2.5,2.5,0.02)
        Q1 = []
        U = []
        I = []
        G = []
        for r in rang:
            x, u, i, g = af.getValueMsingle(r, neuron)
            Q1.append(x)
            U.append(u)
            I.append(i)
            #xd, ud, ud, gd = af.derivative(r, u, i)
            #G.append(xd)
        return Q1,G,rang

    def updatePoints(self, d, I, G, af, lambdQ):
        npr, mpr = np.shape(d)     
        grad2 = np.zeros((npr*mpr, af.size))
        
        # Multiply G2 by the error (reshaped as a vector)
        # Note the use of broadcasting here
        grad_tmp = (G*d.reshape(d.size, 1))
        
        # Vectorize the indices
        lin_indices = I.ravel().astype('int')
        
        # Assign sequentially all gradients
        grad2[np.arange(npr*mpr), lin_indices] = grad_tmp[:, 0]
        grad2[np.arange(npr*mpr), lin_indices+1] = grad_tmp[:, 1]
        grad2[np.arange(npr*mpr), lin_indices+2] = grad_tmp[:, 2]
        grad2[np.arange(npr*mpr), lin_indices+3] = grad_tmp[:, 3]
        
        # Sum per rows
        grad2Rows, grad2Cols = np.shape(grad2)
        grad = np.sum(np.reshape(grad2, ((npr, af.size*(grad2Rows/npr)))), axis = 0)

        grad = grad.reshape(grad2Rows/npr, af.size)
        grad = grad + lambdQ*(af.ys-af.ys0)/(af.ys.size)
        return grad
            
    def testPass(self, inDataTest, inDataTestN, testErr, frac, af1, af2):
        n,m = np.shape(inDataTestN)
        preH = np.concatenate((inDataTestN, np.ones((n, 1))), axis=1).dot(self.inW)
        npr, mpr = np.shape(preH)
        H, U1, I1, G1 = af1.getValueM(preH)
        H = H*frac
        preoutL = np.concatenate((H, np.ones((npr, 1))), axis=1).dot(self.outW)
        outL, U2, I2, G2 = af2.getValueM(preoutL)
        testErr.append(np.sqrt(mean_squared_error(inDataTest,outL)))

    def testPassStack(self, inDataTest, frac, af1):
        n,m = np.shape(inDataTest)
        preH = np.concatenate((inDataTest, np.ones((n, 1))), axis=1).dot(self.inW)
        npr, mpr = np.shape(preH)
        H, U1, I1, G1 = af1.getValueM(preH)
        H = H*frac  
        return H
    
    def FinaltestPass(self, inDataTest, inDataTestN, testErr, frac, af1, af2, dic):
        n,m = np.shape(inDataTestN)
        preH = np.concatenate((inDataTestN, np.ones((n, 1))), axis=1).dot(self.inW)
        npr, mpr = np.shape(preH)
        H, U1, I1, G1 = af1.getValueM(preH)
        H = H*frac
        preoutL = np.concatenate((H, np.ones((npr, 1))), axis=1).dot(self.outW)
        outL, U2, I2, G2 = af2.getValueM(preoutL)
        dic['FinalTest'].append(np.sqrt(mean_squared_error(inDataTest,outL)))
        print "RMSE Test: ", np.sqrt(mean_squared_error(inDataTest,outL))
        
  
    def fPass(self, batchIndex, batchSize, af1, af2, frac):
        self.preH = np.concatenate((self.inData[batchIndex], np.ones((batchSize, 1))), axis=1).dot(self.inW)
        #print "preH ", preH
        npr, mpr = np.shape(self.preH)
        
        #vectorized forward pass layer 1
        self.H, self.U1, self.I1, self.G1 = af1.getValueM(self.preH)
        mask1 = np.random.binomial(1, frac, size=(npr,mpr))
        self.H = self.H*mask1*(1/frac)
        self.preoutL = np.concatenate((self.H, np.ones((batchSize, 1))), axis=1).dot(self.outW)
        #vectorized forward pass layer 2
        self.outL, self.U2, self.I2, self.G2 = af2.getValueM(self.preoutL)
        
        self.error = (self.outL - self.inData[batchIndex])/(batchSize*self.outL.shape[1])
        if self.saveobjF:
            W = np.concatenate((self.inW.flatten(), self.outW.flatten()), axis=0)
            Q = np.concatenate((af1.ys-af1.ys0, af2.ys-af2.ys0), axis = 0)
            objF.append(np.sum(np.power(self.inData[batchIndex]-self.outL,2))/(2.0*batchSize) + self.lambd*np.linalg.norm(W)/(2.0*W.size) + self.lambdQ*np.linalg.norm(Q)/(2.0*Q.size))

    def backPass(self, batchIndex, batchSize, af1, af2, Gk1, Gk2, Gq1, Gq2, err, err2):
        #output-hidden layer gradients
        dx2 = af2.derivativeM(self.preoutL, self.U2, self.I2)

        grad = self.updatePoints(self.error, self.I2, self.G2, af2, self.lambdQ)
        #Adagrad for ctrl points
        sigma = 0.0000001
        Gq2 = Gq2 + grad**2          
        af2.ys += -0.01/(np.sqrt(Gq2)+sigma)*(grad)
        
        #Vanilla SGD for ctrl points
        #af2.ys -= 0.05*grad
        #calculate delta for out-hidden connections
        delta = self.error*(dx2)
        outL_grad = (np.concatenate((self.H, np.ones((batchSize, 1))), axis=1)).T.dot(delta) + self.lambd*self.outW/(self.outW.size)   
        #hidden-input layer gradients
        npr, mpr = np.shape(self.preH)
        dx1 = np.empty([npr, mpr])
     
        #multiply delta with the hidden weights matrix except bias connection
        delta = (delta.dot(self.outW.T[:,:self.nHidden]))
        grad = np.zeros(af1.ys.size)
        
        dx1 = af1.derivativeM(self.H, self.U1, self.I1)


        grad = self.updatePoints(delta, self.I1, self.G1, af1, self.lambdQ)
        #adagrad for ctrl points   
        Gq1 = Gq1 + grad**2        
        af1.ys = af1.ys -0.01/(np.sqrt(Gq1)+sigma)*(grad)          
        
        #vanilla SGD for ctrl points
        #af1.ys -= 0.05*grad
        
        #update delta for hidden input connections
        delta = delta*dx1
        Hgrad = (np.concatenate((self.inData[batchIndex], np.ones((batchSize, 1))), axis=1).T.dot(delta)) + self.lambd*self.inW/(self.inW.size)
        
        #vanilla SGD update  
        #a.inW -= 0.0001*0.5*(Hgrad)
        #a.outW -= 0.0001*0.5*(outL_grad)
        
        Gk1 += Hgrad**2
        self.inW = self.inW -0.01/(np.sqrt(Gk1)+sigma)*(Hgrad)
        
        Gk2 += outL_grad**2
        self.outW = self.outW -0.01/(np.sqrt(Gk2)+sigma)*(outL_grad)
                       
        err.append((mean_squared_error(self.inData[batchIndex], self.outL)))
        err2.append(np.sqrt(((self.inData[batchIndex]-self.outL)**2).mean()))
        return Gk1, Gk2, Gq1, Gq2, err, err2

    def train(self, inData, inDataTest, af1, af2, nHidden):
        frac = 1.0
        #for test in range(0, 3):
        #3-fold-cross validation repeated for number of variable test
        piece = inData.shape[0]
        err = []
        err2 = []
        iters = 2500 #epochs
        n, m = inData.shape
        batchSize = piece/5  
        indexBatch = np.arange(0,n)
        Gk1 = 0
        Gk2 = 0
        Gq1 = 0
        Gq2 = 0
        testErr = []
        saveobjF = 0
        for k in range(0, iters):
            if k%500 == 0:
                print k
            np.random.shuffle(indexBatch)
            inDataTestN = inDataTest
            self.testPass(inDataTest, inDataTestN, testErr, frac, af1, af2)
            for b in range(0, n/batchSize):
                batchIndex = indexBatch[batchSize*b:batchSize*(b+1)]
                #forward pass
                self.fPass(batchIndex, batchSize, af1, af2, frac)
                H = self.H
                preoutL = self.preoutL
                #backward pass  
                Gk1, Gk2, Gq1, Gq2, err, err2 = self.backPass(batchIndex, batchSize, af1, af2, Gk1, Gk2, Gq1, Gq2, err, err2)
        #a.FinaltestPass(inDataFinal, inDataFinalN, testErr, frac, af1, af2, dic)
        test = 0
        print "Test n: ", test, " lambdaQ: ", lambdQ, " lambda: ", lambd
        print "MSE train: ", err[len(err)-1]
        print "RMSE train: ", err2[len(err2)-1]
        print "RMSE validation: ", testErr[len(testErr)-1]
        print "Min RMSE validation: ", min(testErr)
        #state.append(np.random.get_state)
        #testValues.append(min(testErr))
        return self.inW, H, af1
    
frac = 1.0
D = DataClass
inData, inDataTest, Y, YTest = D.load('digits')
np.random.seed(1)
nHidden = 100
lambd = 0.05
lambdQ = 0.05
af1 = SplineAF_ALL(2.5, 0.5, 'CR', nHidden)
af2 = SplineAF_ALL(2.5, 0.5, 'CR', inData.shape[1])
ae = Autoencoder(inData, nHidden, lambd, lambdQ, 0)
W1, H1, af1L = ae.train(inData, inDataTest, af1, af2, nHidden)
L1Test = ae.testPassStack(inDataTest, frac, af1L)


nHidden = 64
lambd = 0.01
lambdQ = 0.01

L1, U, I, G = af1L.getValueM(np.hstack((inData, np.ones((inData.shape[0], 1)))).dot(W1))

af1 = SplineAF_ALL(2.5, 0.5, 'CR', nHidden)
af2 = SplineAF_ALL(2.5, 0.5, 'CR', L1.shape[1])

ae = Autoencoder(L1, nHidden, lambd, lambdQ, 0)
W2, H2, af2L = ae.train(L1, L1Test, af1, af2, nHidden)
L2Test = ae.testPassStack(L1Test, frac, af2L)

Wout = np.random.sample(H2.shape[1]+1)*0.001
RMSETrain = []
RMSETest = []
Gk = 0
L2, U, I, G = af2L.getValueM(np.hstack((L1, np.ones((L1.shape[0], 1)))).dot(W2))

classifier = LogisticRegression(C=1.0, penalty='l2', tol=0.1, multi_class='multinomial', solver='newton-cg')
classifier.fit(L2, Y)
print metrics.accuracy_score(Y, classifier.predict(L2))
print metrics.accuracy_score(YTest, classifier.predict(L2Test))

"""
for i in range(0, 5000):
    preout = np.hstack((L2, np.ones((L2.shape[0], 1)))).dot(Wout)
    out = (preout)
    error = out - Y[:, 0]
    grad = np.hstack((L2, np.ones((L2.shape[0], 1)))).T.dot(error)
    Gk += grad**2
    Wout += -0.01/(np.sqrt(Gk)+0.0000001)*(grad)
    RMSETrain.append(np.sqrt((mean_squared_error(Y, out))/np.var(Y)))
    outT = np.hstack((L2Test,np.ones((L2Test.shape[0], 1)))).dot(Wout)
    RMSETest.append(np.sqrt((mean_squared_error(YTest, outT))/np.var(YTest)))
    
"""
"""
print "NRMSE Train ", RMSETrain[len(RMSETrain)-1]
outT = np.hstack((L2Test,np.ones((L2Test.shape[0], 1)))).dot(Wout)
print "NRMSE Test ", np.sqrt((mean_squared_error(YTest,outT))/np.var(YTest))
"""