# -*- coding: utf-8 -*-
import numpy as np
class SplineAF:
    
    def __init__(self,limit,sampling,Spline_Type):
        #column vector of control points
    
        self.xs = np.arange(-limit,limit+sampling,sampling)
        self.size = np.size(self.xs) 
        self.ys = np.empty(self.size)
        self.ys0 = np.empty(self.size)
        self.sampling = sampling
        if Spline_Type == 'CR':
            self.C = np.array([[-1.0,3.0,-3.0,1.0],[2.0,-5.0,4.0,-1.0],[-1.0,0.0,1.0,0.0],[0.0,2.0,0.0,0.0]])*0.5
        if Spline_Type == 'B':    
            self.C = np.array([[-1.0,  3.0, -3.0,  1.0], [3.0, -6.0,  3.0,  0.0], [-3.0,  0.0,  3.0,  0.0], [1.0,  4.0,  1.0,  0.0]])*(1.0/6.0)
        for i in range(0,self.size):
            self.ys[i] = np.tanh(self.xs[i])
            self.ys0[i] = np.tanh(self.xs[i])
        
    def getI(self,s):

        l = np.size(self.ys)
        Su = s/self.sampling + (l-1)/2       
        i = np.floor(Su)       
        u = Su - i
        i = i - 1 
        
        if i<0:
            i = 0
            u = 0
        if (i>(l-4)):
            i = l-4
            u = 1
        
        return (u,i)             
            
    def getIM(self,s):

        l = np.size(self.ys)
        Su = s/self.sampling + (l-1)/2       
        i = np.floor(Su)       
        u = Su - i
        i = i - 1 
        i = i.clip(0, l-4)
        """
        if i<0:
            i = 0
        if (i>(l-4)):
            i = l-4
        """
        return (u,i) 
        
    def getValue(self,s):
        u,i = self.getI(s)
        g = np.array(self.C.T.dot([u**3, u**2, u, 1]))
        x = g.dot((self.ys[i:i+4]))
        return (x,u,i,g)
    
    #takes as input a matrix s and does the spline interpolation and returns the post activated x
    def getValueM(self,s):
        u,i = self.getIM(s)
     
        u_flat = u.flatten()          
        U = np.array([u_flat**3, u_flat**2, u_flat, np.ones(u.size)]).T           
        g = self.C.T.dot(U.T)
        """
        row, col = np.shape(i)
        index = 0             
        YS = np.empty((i.size, 4))       
        for r in range(0,row):
            for c in range(0,col):
                ii = i[r,c]
                YS[index] = np.array(self.ys[ii:ii+4])
                index+=1
        """
        
        i_flat = i.flatten().astype(int)
        i_flat = np.array([i_flat, i_flat+1, i_flat+2, i_flat+3])
        YS = np.array([self.ys[i_flat]]).T[:,:,0]
        g = g.T        
        x = g*YS
        """
        sum each row to finish vectorizing vector scalar product
        """
        x = x.sum(axis=1)
        x = x.reshape((np.shape(s)))
    
        return (x,u,i,g)    
    
    
    def derivative(self,s,u,i):
            # Compute the derivative of the spline.
            # Additional output arguments are:
            #- u, uIndex: identical to compute_value(s).
            #- g: derivative coordinate vector given by [3u^2 2u 1 0].
            # It is possible to provide u and uIndex as additional
            # arguments to speed up the computation, if they have already
            # been computed previously.
        g = np.array([3*u**2, 2*u, 1, 0]).T.dot(self.C)
        dx = g.dot(self.ys[i:i+4])/self.sampling
        return (dx,u,i,g)
        
    def derivativeM(self,s,u,i):
            # Compute the derivative of the spline.
            # Additional output arguments are:
            #- u, uIndex: identical to compute_value(s).
            #- g: derivative coordinate vector given by [3u^2 2u 1 0].
            # It is possible to provide u and uIndex as additional
            # arguments to speed up the computation, if they have already
            # been computed previously.

        u_flat = u.flatten()
        U = np.array([3*u_flat**2, 2*u_flat, np.ones(u.size), np.zeros(u.size)]).T
                   
        g = self.C.T.dot(U.T)
        #g = np.array([3*u**2, 2*u, 1, 0]).T.dot(self.C)
        #dx = g.dot(self.ys[i:i+4])/self.sampling
        """
        row, col = np.shape(i)
        index = 0             
        YS = np.empty((i.size, 4))       
        for r in range(0,row):
            for c in range(0,col):
                ii = i[r,c]
                YS[index] = np.array(self.ys[ii:ii+4])
                index+=1
                
        """        
        i_flat = i.flatten().astype(int)
        i_flat = np.array([i_flat, i_flat+1, i_flat+2, i_flat+3])
        YS = np.array([self.ys[i_flat]]).T[:,:,0]
        g = g.T        
        dx = g*YS/self.sampling
        """
        sum each row to finish vectorizing vector scalar product
        """
        dx = dx.sum(axis=1)
        dx = dx.reshape((np.shape(s)))        
        
        return (dx)