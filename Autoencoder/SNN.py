# -*- coding: utf-8 -*-
"""
Created on Thu May 05 11:26:21 2016

@author: thewa
"""

from AutoencoderSp import Autoencoder
from sklearn import preprocessing
from sklearn.metrics import mean_squared_error
from sklearn.datasets import load_digits
import Noise as N
import numpy as np

#iris = datasets.load_iris()
#inData = iris.data
"""
bodyfat = scipy.io.loadmat('matlab.mat')
inData = bodyfat['x']
inData = inData.T

"""
digits = load_digits()
inData = digits.data[0:1000]

inDataN = inData
n,m = inData.shape
scalerD = preprocessing.MinMaxScaler((-0.9,0.9))
scalerN = preprocessing.MinMaxScaler((-0.9,0.9))
inData = scalerD.fit_transform(inData)
inDataN = scalerN.fit_transform(inDataN)

print "adding s&p noise.."
for i in range(0,n):
    inDataN[i] = N.saltpepper(inData[i])
print "Done"
diff = np.abs(inData - inDataN)
diff = diff.flatten()
differents = 0
for i in range(0,diff.size):
    if diff[i] != 0:
        differents +=1
"""
noise = np.random.normal(0, 1, n*m).reshape(n,m)*0.1
inDataN = inData + noise
"""
nHidden = 32
inDataTest = inData[800:1000]
inDataTestN = inDataN[800:1000]
inData = inData[0:800]
inDataN = inDataN[0:800]

a = Autoencoder(inData,inDataN,nHidden)
iters=100
for i in range(0,iters):
    fPass = a.forwardPass()
    a.backwardPass(fPass)

print mean_squared_error(a.inDataTest, a.backwardPass['outL'])

