# -*- coding: utf-8 -*-
"""
Created on Tue Feb 09 10:24:50 2016

@author: Alberto Marinelli
"""
import numpy as np
import pylab as pl 
from scipy.stats import norm
from scipy.special import expit
import scipy.io
from sklearn import datasets
from sklearn import preprocessing
from sklearn.metrics import mean_squared_error
from sklearn.datasets import load_digits
import matplotlib.pyplot as plt
import Noise as N
import time
from sklearn.grid_search import ParameterGrid
from mnist import load_mnist
from sklearn.datasets import load_sample_images


def testPass(a, inDataTest, inDataTestN, testErrAE, frac):
    n,m = inDataTestN.shape
    preH = np.concatenate((inDataTestN,np.ones((n,1))),axis=1).dot(a.inW)
    H = np.tanh(preH)
    H *= frac
    preoutL = np.concatenate((H,np.ones((n,1))),axis=1).dot(a.outW)
    outL = np.tanh(preoutL)
    testErrAE.append(np.sqrt(mean_squared_error(inDataTest,outL)))

def grad_activation(n):
    #sigmoid    
    #return expit(n)*(1-expit(n))
    #tanh
    y = np.tanh(n)
    return (1-y**2)

class Autoencoder:
    def __init__(self, inputData, nHidden):
        self.inData = inputData
        n,m = self.inData.shape
        self.inW = np.random.random_sample((m+1,nHidden))*0.001
        self.outW = np.random.random_sample((nHidden+1,m))*0.001
      
#np.random.seed(1)
start_time = time.clock()
saveObjF = 0
db = 'chemical'

if db == 'iris':
    iris = datasets.load_iris()
    inData = iris.data
    inDataN = inData
    scalerD = preprocessing.MinMaxScaler((-0.8,0.8))
    scalerN = preprocessing.MinMaxScaler((-0.8,0.8))
    inData = scalerD.fit_transform(inData)
    inDataN = scalerN.fit_transform(inDataN)
if db == 'bodyfat':
    bodyfat = scipy.io.loadmat('matlab.mat')
    inData = bodyfat['x'].T[0:210]
    inDataN = inData
    scalerD = preprocessing.MinMaxScaler((-0.8,0.8))
    scalerN = preprocessing.MinMaxScaler((-0.8,0.8))
    inData = scalerD.fit_transform(inData)
    inDataN = scalerN.fit_transform(inDataN)
if db == 'digits':
    #lambda for digits: 0.0001
    digits = load_digits()
    inData = digits.data[0:1500]
    inDataN = inData
    scalerD = preprocessing.MinMaxScaler((0,1))
    scalerN = preprocessing.MinMaxScaler((0,1))
    inData = scalerD.fit_transform(inData)
    inDataN = scalerN.fit_transform(inDataN)       
if db == 'mnist': #60000x784 + 10000x784
    num_images = 1000
    inData = load_mnist()[0][0:num_images].reshape(num_images,784).astype(np.float64)
    inData /= 255
    inDataN = inData
if db == 'chemical': #chemical is 498x8
    chem = scipy.io.loadmat('MATLAB_chemical.mat')
    inData = chem['X'][0:400]
    inDataN = inData
    scalerD = preprocessing.MinMaxScaler((-0.8,0.8))
    scalerN = preprocessing.MinMaxScaler((-0.8,0.8))
    inData = scalerD.fit_transform(inData)
    inDataN = scalerN.fit_transform(inDataN)

n,m = inData.shape

"""
print "adding s&p noise.."
for i in range(0,n):
    inDataN[i] = N.saltpepper(inData[i],-0.8,0.8)
print "Done"
diff = np.abs(inData - inDataN)
diff = diff.flatten()
differents = 0
for i in range(0,diff.size):
    if diff[i] != 0:
        differents +=1
"""
"""
noise = np.random.normal(0, 1, n*m).reshape(n,m)*0.01
inDataN = inData + noise
"""
#add bias as +1 dimensional feature
#inDatab = np.concatenate((inDataN,np.ones((n,1))),axis=1)
nHidden = 15
frac = 1
fracIN = 1
#nHidden = nHidden/frac
inDataFull = inData
inDataFullN = inDataN
dic = {'Total idx': [], 'Testnum': [], 'MinTestVal': [], 'Lambda': []}
param_grid = {'Lambda': [0.1, 0.01, 0.001, 0.0001, 0.00001]}
grid = ParameterGrid(param_grid)
count = 0
for params in grid:
    dic['Total idx'].append(count)
    count+=1
    lambd = params['Lambda']
    for test in range(0, 3):
        #3-fold-cross validation repeated for number of variable tests
        ind = np.arange(0, 3)
        piece = 70
        mintests = []
        dic['Lambda'].append(lambd)
        dic['Testnum'].append(test)
        np.random.seed(test)               #keeping the same shifts for both sets
        np.random.shuffle(inDataFull)   #keeping the same shifts for both sets
        np.random.seed(test)               #keeping the same shifts for both sets
        np.random.shuffle(inDataFullN)  #keeping the same shifts for both sets
        np.random.RandomState()         #setting a random state
        for kval in ind:
            inData = inDataFull
            inDataN = inDataFullN
            if kval == 0:
                inDataTest = inData[piece*(kval+2):piece*(kval+3)]
                inDataTestN = inDataN[piece*(kval+2):piece*(kval+3)]
                inData = inData[piece*kval:piece*(kval+2)]
                inDataN = inDataN[piece*kval:piece*(kval+2)]
    
            if kval == 1:
                inDataTest = inData[0:piece]
                inDataTestN = inDataN[0:piece]
                inData = inData[piece*kval:piece*(kval+3)]
                inDataN = inDataN[piece*kval:piece*(kval+3)]            
    
            if kval == 2:
                inDataTest = inData[piece:piece*kval]
                inDataTestN = inDataN[piece:piece*kval]
                inData = np.concatenate((inData[0:piece],inData[piece*(kval):piece*(kval+1)]))
                inDataN = np.concatenate((inDataN[0:piece],inDataN[piece*(kval):piece*(kval+1)]))
            batchSize = piece/5
            a = Autoencoder(inDataN, nHidden)
            #scipy.io.savemat('test.mat', dict(inW=inW))
            iters = 2500
            objFAE = []
            errAE = []
            errAE2 = []
            Gk1 = 0
            Gk2 = 0
            n,m = inData.shape
            testErrAE = []
            indexBatch = np.arange(0,n)
            for k in range(0,iters):            
                np.random.shuffle(indexBatch)
                maskIN = np.random.binomial(1, fracIN, size=(a.inData.shape))
                testPass(a, inDataTest, inDataTestN, testErrAE, frac)
                for b in range(0,n/batchSize):
            #        print a.inW
            #        print a.outW
                    batchIndex = indexBatch[batchSize*b:batchSize*(b+1)]
                    """
                    forward pass
                    """       
                    preH = np.concatenate((a.inData[batchIndex]*maskIN[batchIndex],np.ones((batchSize,1))),axis=1).dot(a.inW)        
            #        print "preH ", preH      
                    npr,mpr = preH.shape
                    #mask = np.random.binomial(1, frac, size=mpr)
                    #mask = np.repeat(mask[np.newaxis,:], npr, 0)
                    mask1 = np.random.binomial(1, frac, size=(npr,mpr))
                    H = np.tanh(preH)
                    H*=mask1
                    
            #        print "H ", H
                    preoutL = np.concatenate((H,np.ones((batchSize,1))),axis=1).dot(a.outW)
            #        print "preoutL ", preoutL
                    outL = np.tanh(preoutL)
            #        print "outL ", outL
                    error = (outL - a.inData[batchIndex])/(batchSize*outL.shape[1])
                    if saveObjF:
                        W = np.concatenate((a.inW.flatten(), a.outW.flatten()), axis=0)
                        objFAE.append(np.sum(np.power(inData[batchIndex]-outL,2))/(batchSize*2.0) + lambd*np.linalg.norm(W)/(2.0*W.size))
                                        
                    """
                    backward pass
                    """
                    #output-hidden layer gradients
                    delta = error*(grad_activation((preoutL)))
                    outL_grad = (np.concatenate((H,np.ones((batchSize,1))),axis=1)).T.dot(delta) + lambd*a.outW/a.outW.size  
            
                    #hidden-input layer gradients
                    delta = (delta.dot(a.outW.T[:,:nHidden]))
                    delta = delta*grad_activation(preH)
            #        print "grad_activation(preH) ", grad_activation(preH)
            #        print "delta ", delta
                    Hgrad = (np.concatenate((a.inData[batchIndex]*maskIN[batchIndex],np.ones((batchSize,1))),axis=1).T.dot(delta)) + lambd*a.inW/a.inW.size 
                    """
                    #0.5 simplified
                    a.outW -= 0.0001*0.5*(outL_grad) 
                    a.inW -= 0.0001*0.5*(Hgrad)
                    """
                    sigma = 0.00001
                    Gk1 += Hgrad**2
                    a.inW += -0.01/(np.sqrt(Gk1)+sigma)*(Hgrad)
                    
                    Gk2 += outL_grad**2
                    a.outW += -0.01/(np.sqrt(Gk2)+sigma)*(outL_grad)
                    
                    
                    errAE.append(mean_squared_error(inData[batchIndex],outL))
                    errAE2.append(np.sqrt(mean_squared_error(inData[batchIndex],outL)))
            print "Test n: ", test, "lambda: ", lambd
            print "MSE train: ", errAE[len(errAE)-1]
            print "RMSE train: ", errAE2[len(errAE)-1]
            print "RMSE test: ", testErrAE[len(testErrAE)-1]
            mintests.append(min(testErrAE))
            #print "FVAL: ", objFAE[len(objFAE)-1]
            #np.random.seed(1)
        acc=0.0
        for te in range(0, len(mintests)):
            acc+= mintests[te]
        dic['MinTestVal'].append(acc/(len(mintests)))
        

"""
plt.figure()
plt.plot(testErrAE, label= "RMSE Test")
plt.plot(errAE, label= "MSE Train")
plt.plot(errAE2, label= "RMSE Train")
plt.legend()
plt.show()
"""
print("--- %s seconds ---" % (time.clock() - start_time))