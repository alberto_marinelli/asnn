# -*- coding: utf-8 -*-
"""
Created on Wed Jul 06 12:57:05 2016

@author: thewa
"""

"""
Created on Tue Feb 09 10:24:50 2016

@author: Alberto Marinelli
"""
import numpy as np
from sklearn import metrics
from sklearn.metrics import mean_squared_error
from AutoencoderClass import AutoencoderClass
import DataClass
from sklearn.linear_model import LogisticRegression
def testPass(a, inDataTest, testErrAE):
    n,m = inDataTest.shape
    preH = np.concatenate((inDataTest,np.ones((n,1))),axis=1).dot(a.inW)
    H = np.tanh(preH)
    preoutL = np.concatenate((H,np.ones((n,1))),axis=1).dot(a.outW)
    outL = np.tanh(preoutL)
    testErrAE.append(np.sqrt(mean_squared_error(inDataTest,outL)))
    return H, np.sqrt(mean_squared_error(inDataTest,outL))

def testPassStack(inDataTest, W1, testErrAE):
    n,m = inDataTest.shape
    preH = np.concatenate((inDataTest,np.ones((n,1))),axis=1).dot(W1)
    H = np.tanh(preH)
    return H

def grad_activation(n):
    #sigmoid    
    #return expit(n)*(1-expit(n))
    #tanh
    y = np.tanh(n)
    return (1-y**2)

def train(inData, inDataTest, nHidden, lambd, a):
    saveObjF = 0
    frac = 1
    fracIN = 1
    #nHidden = nHidden/frac
    #3-fold-cross validation repeated for number of variable tests
    
    piece = inData.shape[0]
    batchSize = piece/5
    #scipy.io.savemat('test.mat', dict(inW=inW))
    iters = 2500
    objFAE = []
    errAE = []
    errAE2 = []
    Gk1 = 0
    Gk2 = 0
    n,m = inData.shape
    testErrAE = []
    indexBatch = np.arange(0,n)
    for k in range(0,iters):            
        np.random.shuffle(indexBatch)
        maskIN = np.random.binomial(1, fracIN, size=(a.inData.shape))
        testPass(a, inDataTest, testErrAE)
        for b in range(0,n/batchSize):
    #        print a.inW
    #        print a.outW
            batchIndex = indexBatch[batchSize*b:batchSize*(b+1)]
            """
            forward pass
            """       
            preH = np.concatenate((a.inData[batchIndex]*maskIN[batchIndex],np.ones((batchSize,1))),axis=1).dot(a.inW)        
    #        print "preH ", preH      
            npr,mpr = preH.shape
            #mask = np.random.binomial(1, frac, size=mpr)
            #mask = np.repeat(mask[np.newaxis,:], npr, 0)
            mask1 = np.random.binomial(1, frac, size=(npr,mpr))
            H = np.tanh(preH)
            H*=mask1
            
    #        print "H ", H
            preoutL = np.concatenate((H,np.ones((batchSize,1))),axis=1).dot(a.outW)
    #        print "preoutL ", preoutL
            outL = np.tanh(preoutL)
    #        print "outL ", outL
            error = (outL - a.inData[batchIndex])/(batchSize*outL.shape[1])
            if saveObjF:
                W = np.concatenate((a.inW.flatten(), a.outW.flatten()), axis=0)
                objFAE.append(np.sum(np.power(inData[batchIndex]-outL,2))/(batchSize*2.0) + lambd*np.linalg.norm(W)/(2.0*W.size))
                                
            """
            backward pass
            """
            #output-hidden layer gradients
            delta = error*(grad_activation((preoutL)))
            outL_grad = (np.concatenate((H,np.ones((batchSize,1))),axis=1)).T.dot(delta) + lambd*a.outW/a.outW.size  
    
            #hidden-input layer gradients
            delta = (delta.dot(a.outW.T[:,:nHidden]))
            delta = delta*grad_activation(preH)
    #        print "grad_activation(preH) ", grad_activation(preH)
    #        print "delta ", delta
            Hgrad = (np.concatenate((a.inData[batchIndex]*maskIN[batchIndex],np.ones((batchSize,1))),axis=1).T.dot(delta)) + lambd*a.inW/a.inW.size 
            """
            #0.5 simplified
            a.outW -= 0.0001*0.5*(outL_grad) 
            a.inW -= 0.0001*0.5*(Hgrad)
            """
            
            sigma = 0.00001
            #adagrad          
            Gk1 += Hgrad**2
            a.inW += -0.01/(np.sqrt(Gk1)+sigma)*(Hgrad)
            
            Gk2 += outL_grad**2
            a.outW += -0.01/(np.sqrt(Gk2)+sigma)*(outL_grad)
            
            """
            #adam
            b1 = 0.9
            b2 = 0.999
            mt1 = b1*mt1_1 + (1-b1)*Hgrad
            vt1 = b2*vt1_1 + (1-b2)*np.power(Hgrad,2)
            mt1_1 = mt1
            vt1_1 = vt1
            Mt1 = mt1/(1-b1)
            Vt1 = vt1/(1-b2)
            
            mt2 = b1*mt2_1 + (1-b1)*outL_grad
            vt2 = b2*vt2_1 + (1-b2)*np.power(outL_grad,2)
            mt2_1 = mt2
            vt2_1 = vt2
            Mt2 = mt2/(1-b1)
            Vt2 = vt2/(1-b2)
            a.inW += -(0.001/(np.sqrt(Vt1)+sigma))*Mt1
            a.outW += -(0.001/(np.sqrt(Vt2)+sigma))*Mt2
            """           
            
            errAE.append(mean_squared_error(inData[batchIndex],outL))
            errAE2.append(np.sqrt(mean_squared_error(inData[batchIndex],outL)))
    print "Test n: ", 0, "lambda: ", lambd
    print "MSE train: ", errAE[len(errAE)-1]
    print "RMSE train: ", errAE2[len(errAE)-1]
    print "RMSE test: ", testErrAE[len(testErrAE)-1]
    #testValues.append(min(testErrAE))
    preH = np.concatenate((a.inData, np.ones((a.inData.shape[0],1))),axis=1).dot(a.inW)    
    npr,mpr = preH.shape
    H = np.tanh(preH)
    return a.inW, H, a
    
D = DataClass
inData, inDataTest, Y, YTest = D.load('digits')

np.random.seed(1)
nHidden = 100
lambd = 0.01
ae = AutoencoderClass(inData, nHidden)
W1, H1, a = train(inData, inDataTest, nHidden, lambd, ae)
H1Test = testPassStack(inDataTest, W1, [])


nHidden = 64
lambd = 0.1
ae = AutoencoderClass(H1, nHidden)
W2, H2, a = train(H1, H1Test, nHidden, lambd, ae)
H2Test = testPassStack(H1Test, W2, [])




L1 = np.tanh(np.hstack((inData, np.ones((inData.shape[0], 1)))).dot(W1))
L2 = np.tanh(np.hstack((L1, np.ones((L1.shape[0], 1)))).dot(W2))
L1Test = np.tanh(np.hstack((inDataTest, np.ones((inDataTest.shape[0], 1)))).dot(W1))
L2Test = np.tanh(np.hstack((L1Test, np.ones((L1Test.shape[0], 1)))).dot(W2))

RMSETrain = []
RMSETest = []
Gk = 0
Wout = np.random.sample(L2.shape[1]+1)*0.001
#L2 = inData
#L2Test = inDataTest
"""
lb = LabelBinarizer()
YTest = lb.fit_transform(YTest)
w = np.zeros([L2.shape[1],len(np.unique(Y))])
lam = 1
iterations = 1000
learningRate = 0.001
losses = []
for i in range(0,iterations):
    loss,grad = softmax.getLoss(w,L2,Y,lam)
    losses.append(loss)
    w = w - (learningRate * grad)
print loss
print 'Training Accuracy: ', softmax.getAccuracy(L2, Y, w)
print 'Test Accuracy: ', softmax.getAccuracy(L2Test, YTest, w)
"""
#Multinomial Logistic regression

classifier = LogisticRegression(C=1.0, penalty='l2', tol=0.1, multi_class='multinomial', solver='newton-cg')
classifier.fit(L2, Y)
print metrics.accuracy_score(Y, classifier.predict(L2))
print metrics.accuracy_score(YTest, classifier.predict(L2Test))


"""#regression
for i in range(0, 5000):
    preout = np.hstack((L2, np.ones((L2.shape[0], 1)))).dot(Wout)
    out = (preout)
    error = out - Y[:, 0]
    grad = np.hstack((L2, np.ones((L2.shape[0], 1)))).T.dot(error)
    Gk += grad**2
    Wout += -0.01/(np.sqrt(Gk)+0.0000001)*(grad)
    RMSETrain.append(np.sqrt((mean_squared_error(Y, out))/np.var(Y)))
    outT = np.tanh(np.hstack((L2Test,np.ones((L2Test.shape[0], 1)))).dot(Wout))
    RMSETest.append(np.sqrt((mean_squared_error(YTest, outT))/np.var(YTest)))
print "NRMSE Train ", RMSETrain[len(RMSETrain)-1]
outT = np.tanh(np.hstack((L2Test,np.ones((L2Test.shape[0], 1)))).dot(Wout))
print "NRMSE Test ", np.sqrt((mean_squared_error(YTest,outT))/np.var(YTest))
"""

"""
print RMSET
nHidden = 10
lambd = 0.001
W3, H3, a = train(H2, H2Test, nHidden, lambd)
H3Test, RMSET = testPass(a, H2Test, [])
print RMSET
"""