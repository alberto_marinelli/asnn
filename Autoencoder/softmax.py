# -*- coding: utf-8 -*-
"""
Created on Wed Jun 22 12:46:22 2016

@author: thewa
"""
import numpy as np
import scipy
from sklearn import preprocessing
from sklearn.preprocessing import OneHotEncoder, LabelBinarizer
import scipy.io

def softmax(z):
    z -= np.max(z)
    sm = (np.exp(z).T / np.sum(np.exp(z),axis=1)).T
    return sm


def getLoss(w,x,y,lam):
    m = x.shape[0]#First we get the number of training examples
    lb = LabelBinarizer()
    y_mat = lb.fit_transform(y)
    #y_mat = oneHotIt(y) #Next we convert the integer class coding into a one-hot representation
    scores = np.dot(x,w) #Then we compute raw class scores given our input and current weights
    prob = softmax(scores) #Next we perform a softmax on these scores to get their probabilities
    loss = (-1 / m) * np.sum(y_mat * np.log(prob)) + (lam/2)*np.sum(w*w) #We then find the loss of the probabilities
    grad = (-1 / m) * np.dot(x.T,(y_mat - prob)) + lam*w #And compute the gradient for that loss
    return loss,grad


def getProbsAndPreds(someX, w):
    probs = softmax(np.dot(someX, w))
    preds = np.argmax(probs,axis=1)
    return probs,preds

def getAccuracy(someX, someY, w):
    prob, prede = getProbsAndPreds(someX, w)
    accuracy = np.sum(prede == someY)/(float(len(someY)))
    return accuracy


def trainSoftmax(x, y, testX, testY):
    w = np.zeros([x.shape[1],len(np.unique(y))])
    lam = 1
    iterations = 1000
    learningRate = 1e-5
    losses = []
    for i in range(0,iterations):
        loss,grad = getLoss(w,x,y,lam)
        losses.append(loss)
        w = w - (learningRate * grad)
    print loss
    
    print 'Training Accuracy: ', getAccuracy(x,y, w)
    print 'Test Accuracy: ', getAccuracy(testX,testY, w)