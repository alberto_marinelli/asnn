# -*- coding: utf-8 -*-
"""Created on Fri Mar 11 16:29:50 2016
@author: thewa
"""
import numpy as np
from sklearn.metrics import mean_squared_error
from SplineAF_ALL import SplineAF_ALL
import matplotlib.pyplot as plt
import time
import DataClass
  
class Autoencoder:
    def __init__(self, inputData, nHidden, lambd, lambdQ, saveobjF):
        self.inData = inputData
        self.lambd = lambd
        self.lambdQ = lambdQ
        self.saveobjF = saveobjF
        n, m = self.inData.shape
        self.nHidden = nHidden
        self.inW= np.random.random_sample((m+1, self.nHidden))*0.001
        self.outW = np.random.random_sample((self.nHidden+1, m))*0.001
        
    def getSplineSample(self, af, neuron):
        rang = np.arange(-2.5,2.5,0.02)
        Q1 = []
        U = []
        I = []
        G = []
        for r in rang:
            x, u, i, g = af.getValueMsingle(r, neuron)
            Q1.append(x)
            U.append(u)
            I.append(i)
            #xd, ud, ud, gd = af.derivative(r, u, i)
            #G.append(xd)
        return Q1,G,rang

    def updatePoints(self, d, I, G, af, lambdQ):
        npr, mpr = np.shape(d)     
        grad2 = np.zeros((npr*mpr, af.size))
        
        # Multiply G2 by the error (reshaped as a vector)
        # Note the use of broadcasting here
        grad_tmp = (G*d.reshape(d.size, 1))
        
        # Vectorize the indices
        lin_indices = I.ravel().astype('int')
        
        # Assign sequentially all gradients
        grad2[np.arange(npr*mpr), lin_indices] = grad_tmp[:, 0]
        grad2[np.arange(npr*mpr), lin_indices+1] = grad_tmp[:, 1]
        grad2[np.arange(npr*mpr), lin_indices+2] = grad_tmp[:, 2]
        grad2[np.arange(npr*mpr), lin_indices+3] = grad_tmp[:, 3]
        
        # Sum per rows
        grad2Rows, grad2Cols = np.shape(grad2)
        grad = np.sum(np.reshape(grad2, ((npr, af.size*(grad2Rows/npr)))), axis = 0)

        grad = grad.reshape(grad2Rows/npr, af.size)
        grad = grad + lambdQ*(af.ys-af.ys0)/(af.ys.size)
        return grad
            
    def testPass(self, inDataTest, inDataTestN, testErr, frac, af1, af2):
        n,m = np.shape(inDataTestN)
        preH = np.concatenate((inDataTestN, np.ones((n, 1))), axis=1).dot(self.inW)
        npr, mpr = np.shape(preH)
        H, U1, I1, G1 = af1.getValueM(preH)
        H = H*frac
        preoutL = np.concatenate((H, np.ones((npr, 1))), axis=1).dot(self.outW)
        outL, U2, I2, G2 = af2.getValueM(preoutL)
        testErr.append(np.sqrt(mean_squared_error(inDataTest,outL)))
    
    def FinaltestPass(self, inDataTest, inDataTestN, testErr, frac, af1, af2, dic):
        n,m = np.shape(inDataTestN)
        preH = np.concatenate((inDataTestN, np.ones((n, 1))), axis=1).dot(self.inW)
        npr, mpr = np.shape(preH)
        H, U1, I1, G1 = af1.getValueM(preH)
        H = H*frac
        preoutL = np.concatenate((H, np.ones((npr, 1))), axis=1).dot(self.outW)
        outL, U2, I2, G2 = af2.getValueM(preoutL)
        dic['FinalTest'].append(np.sqrt(mean_squared_error(inDataTest,outL)))
        print "RMSE Test: ", np.sqrt(mean_squared_error(inDataTest,outL))
        
  
    def fPass(self, batchIndex, batchSize, af1, af2, frac):
        self.preH = np.concatenate((self.inData[batchIndex], np.ones((batchSize, 1))), axis=1).dot(self.inW)
        #print "preH ", preH
        npr, mpr = np.shape(self.preH)
        
        #vectorized forward pass layer 1
        self.H, self.U1, self.I1, self.G1 = af1.getValueM(self.preH)
        mask1 = np.random.binomial(1, frac, size=(npr,mpr))
        self.H = self.H*mask1*(1/frac)
        self.preoutL = np.concatenate((self.H, np.ones((batchSize, 1))), axis=1).dot(self.outW)
        #vectorized forward pass layer 2
        self.outL, self.U2, self.I2, self.G2 = af2.getValueM(self.preoutL)
        
        self.error = (self.outL - self.inData[batchIndex])/(batchSize*self.outL.shape[1])
        if self.saveobjF:
            W = np.concatenate((self.inW.flatten(), self.outW.flatten()), axis=0)
            Q = np.concatenate((af1.ys-af1.ys0, af2.ys-af2.ys0), axis = 0)
            objF.append(np.sum(np.power(self.inData[batchIndex]-self.outL,2))/(2.0*batchSize) + self.lambd*np.linalg.norm(W)/(2.0*W.size) + self.lambdQ*np.linalg.norm(Q)/(2.0*Q.size))

    def backPass(self, batchIndex, batchSize, af1, af2, Gk1, Gk2, Gq1, Gq2, err, err2):
        #output-hidden layer gradients
        dx2 = af2.derivativeM(self.preoutL, self.U2, self.I2)

        grad = self.updatePoints(self.error, self.I2, self.G2, af2, self.lambdQ)
        #Adagrad for ctrl points
        sigma = 0.0000001
        Gq2 = Gq2 + grad**2          
        af2.ys += -0.01/(np.sqrt(Gq2)+sigma)*(grad)
        
        #Vanilla SGD for ctrl points
        #af2.ys -= 0.05*grad
        #calculate delta for out-hidden connections
        delta = self.error*(dx2)
        outL_grad = (np.concatenate((self.H, np.ones((batchSize, 1))), axis=1)).T.dot(delta) + self.lambd*self.outW/(self.outW.size)   
        #hidden-input layer gradients
        npr, mpr = np.shape(self.preH)
        dx1 = np.empty([npr, mpr])
     
        #multiply delta with the hidden weights matrix except bias connection
        delta = (delta.dot(self.outW.T[:,:self.nHidden]))
        grad = np.zeros(af1.ys.size)
        
        dx1 = af1.derivativeM(self.H, self.U1, self.I1)


        grad = self.updatePoints(delta, self.I1, self.G1, af1, self.lambdQ)
        #adagrad for ctrl points   
        Gq1 = Gq1 + grad**2        
        af1.ys = af1.ys -0.01/(np.sqrt(Gq1)+sigma)*(grad)          
        
        #vanilla SGD for ctrl points
        #af1.ys -= 0.05*grad
        
        #update delta for hidden input connections
        delta = delta*dx1
        Hgrad = (np.concatenate((self.inData[batchIndex], np.ones((batchSize, 1))), axis=1).T.dot(delta)) + self.lambd*self.inW/(self.inW.size)
        
        #vanilla SGD update  
        #a.inW -= 0.0001*0.5*(Hgrad)
        #a.outW -= 0.0001*0.5*(outL_grad)
        
        Gk1 += Hgrad**2
        self.inW = self.inW -0.01/(np.sqrt(Gk1)+sigma)*(Hgrad)
        
        Gk2 += outL_grad**2
        self.outW = self.outW -0.01/(np.sqrt(Gk2)+sigma)*(outL_grad)
                       
        err.append((mean_squared_error(self.inData[batchIndex], self.outL)))
        err2.append(np.sqrt(((self.inData[batchIndex]-self.outL)**2).mean()))
        return Gk1, Gk2, Gq1, Gq2, err, err2
   

start_time = time.clock()
state = []
np.random.seed(1)
state.append(np.random.get_state)
db = 'digits'

D = DataClass
inData, inDataTest, Y, YTest = D.load('digits')

n, m = inData.shape


"""
print "adding s&p noise.."
for i in range(0,n):
    inDataN[i] = N.saltpepper(inData[i],-0.8,0.8)
print "Done"
diff = np.abs(inData - inDataN)
diff = diff.flatten()
differents = 0
for i in range(0,diff.size):
    if diff[i] != 0:
        differents +=1

"""
"""
noise = np.random.normal(0, 1, n*m).reshape(n, m)*0.01
inDataN = inData + noise
"""
testValues = []
nHidden = 100
frac = 1
inDataN = inData
inDataTestN = inDataTest
inDataFull = inData
inDataFullN = inDataN
tests = 1
for test in range(0,tests):
    lambd = 0.01
    lambdQ = 0.01
    #for test in range(0, 3):
    #3-fold-cross validation repeated for number of variable test
    ind = np.arange(0, 3)
    piece = inData.shape[0]
    """
    mintests = []
    dic['Lambda'].append(lambd)
    dic['LambdaQ'].append(lambdQ)
    dic['Testnum'].append(test)
    np.random.seed(test)               #keeping the same shifts for both sets
    np.random.shuffle(inDataFull)   #keeping the same shifts for both sets
    np.random.seed(test)               #keeping the same shifts for both sets
    np.random.shuffle(inDataFullN)  #keeping the same shifts for both sets
    np.random.RandomState()  
    """ 
    """      #setting a random state
    for kval in ind:
    inData = inDataFull
    inDataN = inDataFullN
    if kval == 0:
        inDataTest = inData[piece*(kval+2):piece*(kval+3)]
        inDataTestN = inDataN[piece*(kval+2):piece*(kval+3)]
        inData = inData[piece*kval:piece*(kval+2)]
        inDataN = inDataN[piece*kval:piece*(kval+2)]
    
    if kval == 1:
        inDataTest = inData[0:piece]
        inDataTestN = inDataN[0:piece]
        inData = inData[piece*kval:piece*(kval+3)]
        inDataN = inDataN[piece*kval:piece*(kval+3)]            
    
    if kval == 2:
        inDataTest = inData[piece:piece*kval]
        inDataTestN = inDataN[piece:piece*kval]
        inData = np.concatenate((inData[0:piece],inData[piece*(kval):piece*(kval+1)]))
        inDataN = np.concatenate((inDataN[0:piece],inDataN[piece*(kval):piece*(kval+1)]))    
    """
    err = []
    err2 = []
    objF = []
    iters = 2500 #epochs
    n, m = inData.shape
    batchSize = piece/5  
    indexBatch = np.arange(0,n)
    Gk1 = 0
    Gk2 = 0
    Gq1 = 0
    Gq2 = 0
    testErr = []
    saveobjF = 0
    a = Autoencoder(inDataN, nHidden, lambd, lambdQ, saveobjF)
    af1 = SplineAF_ALL(2.5, 0.5, 'CR', nHidden)
    af2 = SplineAF_ALL(2.5, 0.5, 'CR', m)
    for k in range(0, iters):
        if k%500 == 0:
            print k
        np.random.shuffle(indexBatch)
        a.testPass(inDataTest, inDataTestN, testErr, frac, af1, af2)
        for b in range(0, n/batchSize):
            batchIndex = indexBatch[batchSize*b:batchSize*(b+1)]
            #forward pass
            a.fPass(batchIndex, batchSize, af1, af2, frac)
            H = a.H
            preoutL = a.preoutL
            #backward pass  
            Gk1, Gk2, Gq1, Gq2, err, err2 = a.backPass(batchIndex, batchSize, af1, af2, Gk1, Gk2, Gq1, Gq2, err, err2)
    #a.FinaltestPass(inDataFinal, inDataFinalN, testErr, frac, af1, af2, dic)

    print "Test n: ", test, " lambdaQ: ", lambdQ, " lambda: ", lambd
    print "MSE train: ", err[len(err)-1]
    print "RMSE train: ", err2[len(err2)-1]
    print "RMSE validation: ", testErr[len(testErr)-1]
    print "Min RMSE validation: ", min(testErr)
    state.append(np.random.get_state)
    testValues.append(min(testErr))
#save splines point and plot errors and splines     
ys2 = af2.ys
xs2 = af2.xs
spline2 = np.vstack((xs2, ys2))   
ys1 = af1.ys
xs1 = af1.xs
ys0 = af1.ys0

test = 0
plt.figure()


for neuron in range(0, 10):
    Q1, G, rang = a.getSplineSample(af1, neuron)
    plt.plot(rang, Q1)#, label="Spline 1 Test n. %s"%test)
    plt.legend()
    plt.figure()
    Q2, G, rang = a.getSplineSample(af2, neuron)
    plt.plot(rang, Q2)#, label="Spline 2 Test n. %s"%test)
    plt.legend()
    plt.figure()

    
inW = a.inW
outW = a.outW
Qs1 = af1.ys
Qs2 = af2.ys

plt.plot(testErr,label="Test Error    lambda: %f" %lambd)
plt.plot(err2,label="Train Error    lambdaQ: %f" %lambdQ)
plt.plot(nHidden ,label="Hidden neurons : %f" %nHidden)
plt.legend()



print("--- %s seconds ---" % (time.clock() - start_time))